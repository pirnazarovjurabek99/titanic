import pandas as pd

def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df

def get_filled():
    df = get_titatic_dataframe()

    # Extract prefix from 'Name' column
    df['Title'] = df['Name'].apply(lambda name: name.split(",")[1].split(".")[0].strip())

    # Select relevant prefixes
    titles = ["Mr", "Mrs", "Miss"]

    # Create empty result list
    result = []

    # Iterative over titles and calculate missing values and medians
    for title in titles:
        median_age = df.loc[df['Title']==title, 'Age'].median()
        missing_values = df.loc[df['Title']==title, 'Age'].isna().sum()
        result.append((title+'.', missing_values, round(median_age)))

    return result
